import javax.servlet.*;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;

//@WebServlet(name = "ServletTest", value = "/ServletTest")
public class ServletTest extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       HttpSession session=request.getSession();
        Integer count =(Integer) session.getAttribute("count");
        PrintWriter printWriter = response.getWriter();
        printWriter.println("<html>");
        if (count==null){
            count=1;
            session.setAttribute("count",count);
            printWriter.println("<h1>Count : "+count+"</h1>");
        }else {
            count++;
            session.setAttribute("count",count);
            printWriter.println("<h1>Count : "+count+"</h1>");
        }
        printWriter.println("</html>");
//        String name=request.getParameter("name");
//       System.out.println(name);

//        response.sendRedirect("https://www.google.com");
//        response.sendRedirect("/helloJsp");
//        RequestDispatcher dispatcher=request.getRequestDispatcher("/helloJsp");
//        dispatcher.forward(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }


}
