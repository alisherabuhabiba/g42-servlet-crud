package task1;

import task1.payload.Student;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

public class ServletGetStudents extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            Class.forName("org.postgresql.Driver");
            Connection connection = DriverManager.getConnection(
                    "jdbc:postgresql://localhost:5432/g42",
                    "postgres",
                    "root123"
            );
            Statement statement = connection.createStatement();
            ResultSet resultSet=statement.executeQuery("select * from student");
            PrintWriter writer = response.getWriter();
            writer.println("<html>");
            writer.println("<a href=\"/saveOrEditStudent\">Add Student</a>");
            writer.println("<table border=\"1\">");
            writer.println("<tr>");
            writer.println("<th>TR</th>");
            writer.println("<th>ID</th>");
            writer.println("<th>Name</th>");
            writer.println("<th>Age</th>");
            writer.println("<th>Edit</th>");
            writer.println("<th>Delete</th>");
            writer.println("</tr>");
            int count=1;
            while (resultSet.next()){
                writer.println("<tr>");
                writer.println("<td>"+count+"</td>");
                writer.println("<td>"+resultSet.getString("id")+"</td>");
                writer.println("<td>"+resultSet.getString("name")+"</td>");
                writer.println("<td>"+resultSet.getString("age")+"</td>");
                writer.println("<td><a href=\"/saveOrEditStudent?id="+resultSet.getString("id")+"\">Edit</a></td>");
                writer.println("<td><a href=\"/deleteStudent?id="+resultSet.getString("id")+"\">Delete</a></td>");
                writer.println("</tr>");
                count++;
            }
            writer.println("</table>");
            writer.println("</html>");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id");
        String name = request.getParameter("name");
        Integer age = Integer.parseInt(request.getParameter("age"));
        try {
            Class.forName("org.postgresql.Driver");
            Connection connection = DriverManager.getConnection(
                    "jdbc:postgresql://localhost:5432/g42",
                    "postgres",
                    "root123"
            );
            Student student = new Student();
            if (id!=null){
                PreparedStatement preparedStatement = connection.prepareStatement("update student set name=?, age=? where id=" + id);
                preparedStatement.setString(1,name);
                preparedStatement.setInt(2,age);
                preparedStatement.executeUpdate();
            }
            else {
                PreparedStatement preparedStatement = connection.prepareStatement("insert into student(name,age) values (?,?)");
                preparedStatement.setString(1,name);
                preparedStatement.setInt(2,age);
                preparedStatement.executeUpdate();

            }
            response.sendRedirect("/getStudents");

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
