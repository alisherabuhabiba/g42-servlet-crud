package task1;

import task1.payload.Student;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

public class ServletSaveOrEditStudent extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id");
        try {
            Class.forName("org.postgresql.Driver");
            Connection connection = DriverManager.getConnection(
                    "jdbc:postgresql://localhost:5432/g42",
                    "postgres",
                    "root123"
            );
            Statement statement = connection.createStatement();
            Student student = new Student();
            if (id!=null){
                ResultSet resultSet = statement.executeQuery("select * from student where id=" + id);
                while (resultSet.next()){
                    student.setId(Integer.parseInt(resultSet.getString("id")));
                    student.setAge(Integer.parseInt(resultSet.getString("age")));
                    student.setName(resultSet.getString("name"));
                }
            }
            PrintWriter writer = response.getWriter();
            writer.println("<html>");
            writer.println("<form action=\"/getStudents\" method=\"post\">");
            if (student.getId()!=null){
                writer.println("<input type=\"hidden\" name=\"id\" value=\""+student.getId()+"\">");
            }
            writer.println("<input name=\"name\" value=\""+student.getName()+"\">");
            writer.println("<input name=\"age\" value=\""+student.getAge()+"\">");
            writer.println("<input type=\"submit\" value=\"Save\">");
            writer.println("</form>");
            writer.println("</html>");

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
